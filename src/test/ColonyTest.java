package test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import common.Colony;
import common.Motel;
import common.Room;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

class ColonyTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    void tearDown() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    void getName() {
        Colony colony = new Colony("Center parc", 100, 200);
        assertEquals("Center parc", colony.getName());
    }

    @Test
    void setName() {
        Colony colony = new Colony("Name to change", 100, 200);
        colony.setName("Center parc");
        assertEquals("Center parc", colony.getName());
    }

    @Test
    void getPopulation() {
        Colony colony = new Colony("Center parc", 100, 200);
        assertEquals(100, colony.getPopulation());
    }

    @Test
    void setPopulation() {
        Colony colony = new Colony("Center parc", 200, 200);
        colony.setPopulation(100);
        assertEquals(100, colony.getPopulation());
    }

    @Test
    void getGrowRate() {
        Colony colony = new Colony("Center parc", 100, 200);
        assertEquals(200, colony.getGrowRate());
    }

    @Test
    void setGrowRate() {
        Colony colony = new Colony("Center parc", 100, 100);
        colony.setGrowRate(200);
        assertEquals(200, colony.getGrowRate());
    }

    @Test
    void notifier() {
        Colony colony = new Colony("Center parc", 100, 100);
        Motel mr = Motel.getInstance();
        mr.createRooms();
        ArrayList<String> commodites = new ArrayList<String>();
        Room r1 = mr.enregistrement(colony, "Suite", commodites);

        colony.notifier(r1, mr);
        assertEquals("Room number : 0\r\n" +
                "Colony : Center parc\r\nCenter parc : Puis-je récupérer la chambre 0 qui s'est libérée ?\r\n", outContent.toString());
    }

}