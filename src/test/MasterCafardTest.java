package test;

import common.MasterCafard;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class MasterCafardTest {

    @Test
    void getCodeSecurite() {
        MasterCafard masterCafard = create();
        assertEquals("1234", masterCafard.getCodeSecurite());
    }

    @Test
    void setCodeSecurite() {
        MasterCafard masterCafard = create();
        masterCafard.setCodeSecurite("12345");
        assertEquals("12345", masterCafard.getCodeSecurite());
    }

    @Test
    void getDateExpiration() {
        MasterCafard masterCafard = create();
        Date expirationDate = createDate();
        assertEquals(expirationDate, masterCafard.getDateExpiration());
    }

    @Test
    void setDateExpiration() {
        MasterCafard masterCafard = create();

        // Create a date that is 1 day after the date we use to create our master cafard card
        Date expirationDate = createDate();
        Calendar c = Calendar.getInstance();
        c.setTime(expirationDate);
        // ADD 1 DAY TO THE DATE created
        c.add(Calendar.DATE, 1);

        expirationDate = c.getTime();

        masterCafard.setDateExpiration(expirationDate);

        assertEquals(expirationDate, masterCafard.getDateExpiration());
    }

    @Test
    void getName() {
        MasterCafard masterCafard = create();
        assertEquals("Michel", masterCafard.getName());
    }

    @Test
    void setName() {
        MasterCafard masterCafard = create();
        masterCafard.setName("I did not hit her");
        assertEquals("I did not hit her", masterCafard.getName());
    }

    @Test
    void getCodeCarte() {
        MasterCafard masterCafard = create();
        assertEquals("123", masterCafard.getCodeCarte());
    }

    @Test
    void setCodeCarte() {
        MasterCafard masterCafard = create();
        masterCafard.setCodeCarte("test123");
        assertEquals("test123", masterCafard.getCodeCarte());
    }

    /**
     * Creates a master cafard card for testing
     *
     * @return master cafard created
     */
    private MasterCafard create() {
        Date dateExpiration = createDate();
        MasterCafard masterCafard = new MasterCafard("123", "Michel", "1234", dateExpiration);
        return masterCafard;
    }

    /**
     * Creates the date used in master cafard creation
     *
     * @return the created date
     */
    public Date createDate() {
        SimpleDateFormat format = new SimpleDateFormat("MM/yyyy");
        String date = "12/21";
        Date dateExpiration = null;
        try {
            dateExpiration = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateExpiration;
    }
}