package test;

import common.Colony;
import common.Motel;
import common.Room;
import common.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class MotelTest {

    /**
     * Resets all the variables that could be altered during the tests
     */
    @AfterEach
    void tearDown() {
        Motel motel = Motel.getInstance();
        ArrayList<Room> rooms = new ArrayList<>();
        ArrayList<Transaction> transactions = new ArrayList<>();
        motel.rooms = rooms;
        motel.transactions = transactions;
        motel.setRoomNbr(2);
        motel.setCapacity(100);
    }


    @Test
    void getInstance() {
        Motel motel = Motel.getInstance();
        Motel motel2 = Motel.getInstance();
        assertEquals(motel, motel2);
    }

    @Test
    void createRooms() {
        Motel motel = Motel.getInstance();
        motel.createRooms();
        assertEquals(motel.getRoomNbr(), motel.rooms.size());
    }

    @Test
    void getFirstAvailableRoom() {
        Motel motel = Motel.getInstance();
        motel.createRooms();

        Colony colony = new Colony("Edgy boiz", 100, 200);
        Colony colony1 = new Colony("Edgy boiz", 100, 200);
        Colony colony2 = new Colony("Edgy boiz", 100, 200);

        // Assert not null if room free
        assertNotNull(motel.getFirstAvailableRoom(colony));

        // We now full the rooms
        ArrayList<String> commodites = new ArrayList<>();
        commodites.add("Mini-bar");
        Room r1 = motel.enregistrement(colony, "Suite", commodites);
        Room r2 = motel.enregistrement(colony1, "Suite", commodites);

        // Assert null when all rooms are taken
        assertNull(motel.getFirstAvailableRoom(colony2));
    }

    @Test
    void paiement() {
        Motel motel = Motel.getInstance();

        // Outputting predefined value for the card
        ByteArrayInputStream in = new ByteArrayInputStream("1\nname\ncardCode".getBytes());
        System.setIn(in);
        motel.paiement(123);

        assertNotNull(motel.transactions.get(0));
    }

    @Test
    void leaveRoom() {
        Motel motel = Motel.getInstance();
        motel.createRooms();

        Colony colony = new Colony("Edgy boiz", 100, 200);

        ArrayList<String> commodites = new ArrayList<>();
        commodites.add("Mini-bar");
        Room r = motel.enregistrement(colony, "Suite", commodites);

        // Outputting predefined value for the card
        ByteArrayInputStream in = new ByteArrayInputStream("1\nname\ncardCode".getBytes());
        System.setIn(in);

        motel.leaveRoom(r);

        assertNull(motel.rooms.get(0).getColony());
    }

    @Test
    void enregistrement() {
        Motel motel = Motel.getInstance();
        motel.createRooms();

        Colony colony = new Colony("Edgy boiz", 100, 200);

        ArrayList<String> commodites = new ArrayList<>();
        commodites.add("Mini-bar");
        Room r = motel.enregistrement(colony, "Suite", commodites);

        assertEquals("Edgy boiz", motel.rooms.get(0).getColony().getName());
    }

    @Test
    void addDays() {
        Motel motel = Motel.getInstance();
        motel.createRooms();

        Colony colony = new Colony("Edgy boiz", 100, 200);

        ArrayList<String> commodites = new ArrayList<>();
        commodites.add("Mini-bar");
        Room r = motel.enregistrement(colony, "Suite", commodites);

        motel.addDays();
        assertEquals(1, r.getDays());
    }

    @Test
    void testAddDays() {
        Motel motel = Motel.getInstance();
        motel.createRooms();

        Colony colony = new Colony("Edgy boiz", 100, 200);

        ArrayList<String> commodites = new ArrayList<>();
        commodites.add("Mini-bar");
        Room r = motel.enregistrement(colony, "Suite", commodites);

        motel.addDays(10);
        assertEquals(10, r.getDays());
    }

    @Test
    void printMotelTransactions() {
        Motel motel = Motel.getInstance();

        // Outputting predefined value for the card
        ByteArrayInputStream in = new ByteArrayInputStream("1\nname\ncardCode".getBytes());
        System.setIn(in);

        // Doing a paiement to create a new transaction
        motel.paiement(123);

        String strPrice = "" + motel.transactions.get(0).getPrice();
        String strClient = "" + motel.transactions.get(0).getName();

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        motel.printMotelTransactions();

        // Check if the price and the client are in the transaction outprint
        assertNotEquals(-1, outContent.toString().indexOf(strPrice));
        assertNotEquals(-1, outContent.toString().indexOf(strClient));
    }


    @Test
    void testToString() {
        Motel motel = Motel.getInstance();
        String expected = "==== " + motel.getName() + " ====\n" +
                "Capacity : " + motel.getCapacity() + "\n" +
                "Number of rooms : " + motel.getRoomNbr() + "\n";

        assertEquals(expected, motel.toString());
    }

    @Test
    void getName() {
        Motel motel = Motel.getInstance();
        assertEquals("Motel de cafards", motel.getName());
    }

    @Test
    void setName() {
        Motel motel = Motel.getInstance();
        motel.setName("Name changed");
        assertEquals("Name changed", motel.getName());
    }

    @Test
    void getCapacity() {
        Motel motel = Motel.getInstance();
        assertEquals(100, motel.getCapacity());
    }

    @Test
    void setCapacity() {
        Motel motel = Motel.getInstance();
        motel.setCapacity(200);
        assertEquals(200, motel.getCapacity());
    }

    @Test
    void getRoomNbr() {
        Motel motel = Motel.getInstance();
        motel.getRoomNbr();
        assertEquals(2, motel.getRoomNbr());
    }

    @Test
    void setRoomNbr() {
        Motel motel = Motel.getInstance();
        motel.setRoomNbr(20);
        assertEquals(20, motel.getRoomNbr());
    }
}