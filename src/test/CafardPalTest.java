package test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import common.*;

import static org.junit.jupiter.api.Assertions.*;

class CafardPalTest {

    @Test
    void getName() {
        CafardPal cafardPal = new CafardPal("123", "Michel");
        assertEquals("Michel", cafardPal.getName());
    }

    @Test
    void setName() {
        CafardPal cafardPal = new CafardPal("123", "Name to change");
        cafardPal.setName("Michel");
        assertEquals("Michel", cafardPal.getName());
    }

    @Test
    void getCodeCarte() {
        CafardPal cafardPal = new CafardPal("123", "Michel");
        assertEquals("123", cafardPal.getCodeCarte());
    }

    @Test
    void setCodeCarte() {
        CafardPal cafardPal = new CafardPal("Code to change", "Michel");
        cafardPal.setCodeCarte("123");
        assertEquals("123", cafardPal.getCodeCarte());
    }
}