package test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import common.Colony;
import common.Motel;
import common.Room;
import common.Suite;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

class RoomCommodityTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    void tearDown() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    void makeFiesta() {
        Colony c1 = new Colony("Center parc 1", 100, 200);
        Room r1 = new Suite(0);
        r1.setColony(c1);
        r1.makeFiesta();
        double result = 100 * 200 / 100;
        result = result * 0.5;
        assertEquals(result, r1.getColony().getPopulation());
    }

    @Test
    void getNum() {
        Room r1 = new Suite(0);
        assertEquals(0, r1.getNum());
    }

    @Test
    void setNum() {
        Room r1 = new Suite(1);
        r1.setNum(0);
        assertEquals(0, r1.getNum());
    }

    @Test
    void getColony() {
        Colony c1 = new Colony("Center parc", 100, 200);
        Room r1 = new Suite(0);
        r1.setColony(c1);
        assertEquals(c1, r1.getColony());
    }

    @Test
    void setColony() {
        Colony c1 = new Colony("Center parc", 100, 200);
        Colony c2 = new Colony("New parc", 200, 100);
        Room r1 = new Suite(0);
        r1.setColony(c1);
        assertEquals(c1, r1.getColony());
        r1.setColony(c2);
        assertEquals(c2, r1.getColony());
    }

    @Test
    void printBill() {
        Room r1 = new Suite(0);
        r1.printBill();
        assertEquals("==== Facture ====\r\nSuite:\t100\r\n", outContent.toString());
    }

    @Test
    void getDays() {
        Room r1 = new Suite(0);
        assertEquals(0, r1.getDays());
    }

    @Test
    void setDays() {
        Room r1 = new Suite(0);
        r1.setDays(5);
        assertEquals(5, r1.getDays());
    }
}