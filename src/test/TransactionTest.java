package test;

import common.CafardPal;
import common.MasterCafard;
import common.Transaction;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TransactionTest {

    @Test
    void getCard() {
        Transaction transaction = create();
        assertEquals("CafardPal", transaction.getCard().getClass().getSimpleName());
    }

    @Test
    void setCard() {
        // Retrieve the createDate method from the master cafard test class
        MasterCafardTest masterCafardTest = new MasterCafardTest();
        Date expirationDate = masterCafardTest.createDate();

        MasterCafard masterCafard = new MasterCafard("codeCarte", "Jean-Louis", "codeSecu", expirationDate);
        Transaction transaction = create();
        transaction.setCard(masterCafard);
        assertEquals(masterCafard, transaction.getCard());
    }

    @Test
    void getPrice() {
        Transaction transaction = create();
        assertEquals(123, transaction.getPrice());
    }

    @Test
    void setPrice() {
        Transaction transaction = create();
        transaction.setPrice(1234);
        assertEquals(1234, transaction.getPrice());
    }

    @Test
    void getName() {
        Transaction transaction = create();
        assertEquals("Jean-Louis", transaction.getName());
    }

    @Test
    void setName() {
        Transaction transaction = create();
        transaction.setName("Mark");
        assertEquals("Mark", transaction.getName());
    }

    @Test
    void testToString() {
        Transaction transaction = create();
        String str = "Type : \t" + transaction.getCard().getClass().getSimpleName() + "\n" +
                "Price : \t" + transaction.getPrice() + "\n" +
                "Client : \t" + transaction.getName() + "\n";
        assertEquals(str, transaction.toString());

    }

    private Transaction create() {
        CafardPal cafardPal = new CafardPal("code123", "Jean-Louis");
        Transaction transaction = new Transaction(cafardPal, 123, cafardPal.getName());
        return transaction;
    }
}