package common;

/**
 * ColonyObserver
 * Design Pattern : Observer
 */
public interface ColonyObserver {
    /**
     * Prints a message when the colony is notified that the room from a certain motel is available
     *
     * @param room the newly available room
     * @param motel the motel from which the room is available
     */
    void notifier(Room room, Motel motel);

    /**
     * Subscribe the colony to a motel
     *
     * @param motel the motel the colony wants to be subscribed for
     */
    void subscribe(Motel motel);
}
