package common;

/**
 * Room Design Pattern : AbstractFactory
 */
public abstract class Room {
    /**
     * Returns the price of the room
     *
     * @return the price of the room
     */
    abstract int price();

    /**
     * Tells if the room has a shower commodity or not
     *
     * @return true if the room has a shower and false otherwise
     */
    abstract boolean douche();

    private int days = 0; // Days passed

    private int num; // Room number
    private Colony colony; // The colony that is living in the room

    /**
     * Changes the colony population that lives inside the room after a party
     */
    public void makeFiesta() {
        double div = 0.5;
        String str = "";

        // Colony population increases after the party
        this.getColony().setPopulation(this.getColony().getGrowRate() / 100 * this.getColony().getPopulation());
        System.out.println("~~~");
        System.out.println("After this epic Fiesta the new " + this.getColony().getName()
                + " population has increased to " + this.getColony().getPopulation());

        // Checks if the room is equipped with a shower
        if (this.douche()) {
            div = 0.75;
            str = "\nLuckily their room was equipped with an anti-insecticide shower \\( ﾟヮﾟ)/ !";
        }

        this.getColony().setPopulation((int) (div * this.getColony().getPopulation()));
        System.out.println(
                "Due to the disturbance of the fiesta, the motel administration decided to use insecticide to reduce the "
                        + this.getColony().getName() + " population to " + this.getColony().getPopulation() + str);
        System.out.println("~~~");
    }


    /**
     * Room constructor used to create a room and attributing the room number to the
     * room
     *
     * @param num the room number
     */
    public Room(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Colony getColony() {
        return colony;
    }

    public void setColony(Colony colony) {
        this.colony = colony;
    }

    /**
     * Prints the bill of the room
     */
    public void printBill() {
        System.out.println("==== Facture ====");
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }
}

// Concrete
// class Standard extends Room {

// public Standard(int num) {
// super(num);
// }

// @Override
// int price() {
// return 50;
// }

// @Override
// boolean douche() {
// return false;
// }

// @Override
// void printBill() {
// super.printBill();
// System.out.println("Standard:\t50");
// }
// }

// Concrete
// class Deluxe extends Room {

// public Deluxe(int num) {
// super(num);
// }

// @Override
// int price() {
// return 75;
// }

// @Override
// boolean douche() {
// return false;
// }

// @Override
// void printBill() {
// super.printBill();
// System.out.println("Deluxe:\t75");
// }
// }

// Concrete
// class Suite extends Room {

// public Suite(int num) {
// super(num);
// }

// @Override
// int price() {
// return 100;
// }

// @Override
// boolean douche() {
// return false;
// }

// @Override
// void printBill() {
// super.printBill();
// System.out.println("Suite:\t100");
// }
// }
