package common;

public class Standard extends Room {

    public Standard(int num) {
        super(num);
    }

    @Override
    int price() {
        return 50;
    }

    @Override
    boolean douche() {
        return false;
    }

    @Override
    public void printBill() {
        super.printBill();
        System.out.println("Standard:\t50");
    }
}