package common;

import java.util.Date;

/**
 * MasterCafard
 * Heritage : Card
 */
public class MasterCafard implements Card {
    private String codeSecurite; // Security code of the card
    private Date dateExpiration; // Expiration date of the card
    private String name; // Name of the card
    private String codeCarte; // Code of the card

    public MasterCafard(String codeCarte, String name, String codeSecurite, Date dateExpiration) {
        this.name = name;
        this.codeCarte = codeCarte;

        this.codeSecurite = codeSecurite;
        this.dateExpiration = dateExpiration;
    }

    public String getCodeSecurite() {
        return codeSecurite;
    }

    public void setCodeSecurite(String codeSecurite) {
        this.codeSecurite = codeSecurite;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeCarte() {
        return codeCarte;
    }

    public void setCodeCarte(String codeCarte) {
        this.codeCarte = codeCarte;
    }
}