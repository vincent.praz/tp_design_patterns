package common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Motel
 * Design Pattern : Singleton, Observable
 */
public class Motel {
    private static Motel motel = null;

    private String name; // Name of the motel
    private int capacity; // Capacity of the motel
    private int roomNbr; // Number of rooms in the motel
    public ArrayList<Room> rooms; // Rooms of the motel
    public ArrayList<Transaction> transactions; // Transaction of the motel

    private List<Colony> listColonyObserver; // List of the colony observers


    private Motel(String name, int capacity, int roomNbr) {
        this.listColonyObserver = new ArrayList<>();
        this.name = name;
        this.capacity = capacity;
        this.roomNbr = roomNbr;

        this.rooms = new ArrayList<>();
        this.transactions = new ArrayList<>();
    }

    /**
     * Adds Colony observers to the motel
     *
     * @param newColonyObserver the new colony observer
     */
    public void subscribe(Colony newColonyObserver) {
        this.listColonyObserver.add(newColonyObserver);
    }

    /**
     * Removes the colony observer of the motel
     *
     * @param colonyObserverToRemove the colony observer to remove
     */
    public void detach(Colony colonyObserverToRemove) {
        this.listColonyObserver.remove(colonyObserverToRemove);
    }

    /**
     * Notifies the colony observers that the room is available
     *
     * @param room the room that is available
     */
    public void notifier(Room room) {
        for (Colony myColonyObserver : listColonyObserver) {
            myColonyObserver.notifier(room, this);
        }
    }

    /**
     * Returns the instance of the current motel or creates a new one if the motel Instance doesn't exist
     *
     * @return the instance of the motel
     */
    public static Motel getInstance() {
        // Check if the motel exists
        if (motel == null) {
            // Create a new instance of the motel if not created
            motel = new Motel("Motel de cafards", 100, 2);
        }
        return motel;
    }


    /**
     * Creates the rooms
     */
    public void createRooms() {
        // Create the number of rooms necessary
        for (int i = 0; i < roomNbr; ++i) {
            Room room = new Room(i) {
                @Override
                int price() {
                    return 0;
                }

                @Override
                boolean douche() {
                    return false;
                }
            };
            this.rooms.add(room);
        }
    }


    /**
     * Gets the first available room
     *
     * @param colony the colony that wants to get in the room
     * @return the first room available
     */
    public Room getFirstAvailableRoom(Colony colony) {
        ArrayList<Room> roomFrees = new ArrayList<>();

        // Create a new arraylist containing the list of free rooms
        for (Room roomFree : this.rooms) {
            if (roomFree.getColony() == null) {
                roomFrees.add(roomFree);
            }
        }

        // If motel full
        if (roomFrees.size() == 0) {
            // Colony subscribe to the motel
            colony.subscribe(motel);
            System.out.println("The motel is full, " + colony.getName() + " is queued");

            return null;
        }

        // If it's the last room free
        if (roomFrees.size() == 1) {
            // Display a sign
            System.out.println("|￣￣￣￣￣￣ |\n" +
                    "|    THE    |\n" +
                    "|   MOTEL   | \n" +
                    "|  IS FULL  |\n" +
                    "|    NOW    |\n" +
                    "| ＿＿＿＿＿__| \n" +
                    "(\\__/) || \n" +
                    "(•ㅅ•) || \n" +
                    "/ 　 づ");
        }


        System.out.println("Room number : " + roomFrees.get(0).getNum());
        return roomFrees.get(0);
    }

    /**
     * Manage the user entries that are required to be able to create a transaction
     * and creates a new transaction entry in the motel transaction list
     *
     * @param price total price expected for the payment
     */
    public void paiement(int price) {
        // Display a message to get the user to chose a payment method
        System.out.println("Choose a payment Method\n1:\t CafardPal\n2:\t MasterCafard");

        Scanner scan = new Scanner(System.in);

        // Retrieve the choice of the user
        int choice = scan.nextInt();

        Card card;
        String name;
        String code;
        Transaction transaction;


        switch (choice) {
            case 1:
                // CafardPal
                // Retrieve all user infos about their CafardPal "card"
                System.out.println("Enter your name");
                name = scan.next();

                System.out.println("Enter your card code");
                code = scan.next();

                card = new CafardPal(code, name);

                // Create and add a new transaction to the motel's transaction list
                transaction = new Transaction(card, price, name);
                this.transactions.add(transaction);

                break;
            case 2:
                // MasterCafard
                // Retrieve all user infos about their MasterCafard card
                System.out.println("Enter your name");
                name = scan.next();

                System.out.println("Enter your card code");
                code = scan.next();

                System.out.println("Enter your card security code");
                String codeSecurite = scan.next();

                System.out.println("Enter your card expiration date (MM/yy) eg. 12/21");
                String date = scan.next();
                SimpleDateFormat format = new SimpleDateFormat("MM/yyyy");

                Date dateExpiration = null;
                try {
                    dateExpiration = format.parse(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                System.out.println(dateExpiration);

                card = new MasterCafard(code, name, codeSecurite, dateExpiration);

                // Create and add a new transaction to the motel's transaction list
                transaction = new Transaction(card, price, name);
                this.transactions.add(transaction);

                break;
            default:
                // Display an error message
                System.out.println("Choose between 1 or 2");

                // Re-call the function if the number is incorrect
                paiement(price);
                break;
        }
    }


    /**
     * Make the colony leave the room and notifies the other colonies that were queued that a room is free
     *
     * @param room room to free
     */
    public void leaveRoom(Room room) {
        System.out.println(room.getColony().getName() + " : Quitte la chambre " + room.getNum());

        // Prints bill when colony leaves room and calculate the total price taking the days into account
        room.printBill();
        System.out.println("==> prix total = " + room.price() + ".- * " + room.getDays() + " Days");
        System.out.println("\t" + room.price() * room.getDays() + ".-\n");


        // Payment
        paiement(room.price() * room.getDays());

        // Resetting the room when leaving
        Room roomReset = new Room(room.getNum()) {
            @Override
            int price() {
                return 0;
            }

            @Override
            boolean douche() {
                return false;
            }
        };

        // Update the rooms arrayList
        updateRooms(roomReset);

        // Notify the queued (subscribed) colonies that a is available
        motel.notifier(roomReset);
    }


    /**
     * Creates a record of a colony into a room, sets the type of the room and adds commodities to the room
     *
     * @param colony          Colony occupying the room
     * @param type            Type of the room (Luxe, Standard, Suite)
     * @param roomCommodities ArrayList containing the room commodities
     * @return room The room Created
     */
    public Room enregistrement(Colony colony, String type, ArrayList<String> roomCommodities) {
        Room room = getFirstAvailableRoom(colony);

        // If no rooms are free
        if (room == null) {
            return null;
        }

        // Change the type of the room
        switch (type) {
            case "Suite":
                room = new Suite(room.getNum());
                break;
            case "Standard":
                room = new Standard(room.getNum());
                break;
            case "Luxe":
                room = new Deluxe(room.getNum());
                break;
        }


        // Remove duplicates in the roomCommodities list
        Set<String> set = new HashSet<>(roomCommodities);
        roomCommodities.clear();
        roomCommodities.addAll(set);

        // Add the commodities into the room
        for (String roomCommodity : roomCommodities) {
            switch (roomCommodity) {
                case "Douche":
                    room = new Douche(room);
                    break;
                case "Mini-bar":
                    room = new MiniBar(room);
                    break;
                case "Spa":
                    room = new Spa(room);
                    break;
            }
        }

        // Set the colony to the room
        room.setColony(colony);

        // Show the colony newly occupying the room
        System.out.println("Colony : " + colony.getName());

        // Update the rooms arraylist
        updateRooms(room);

        return room;
    }

    /**
     * Add 1 day to every rooms
     */
    public void addDays() {
        for (Room room : this.rooms) {
            room.setDays(room.getDays() + 1);
        }
    }

    /**
     * Add a number of days to every rooms
     *
     * @param nbrDays the number of days to be added to the rooms
     */
    public void addDays(int nbrDays) {
        for (Room room : this.rooms) {
            room.setDays(room.getDays() + nbrDays);
        }
    }


    /**
     * Prints the motel transactions
     */
    public void printMotelTransactions() {
        System.out.println("==== Transactions ====");

        for (Transaction transaction : this.transactions) {
            System.out.println(transaction.toString());
        }

        System.out.println("======================");
    }

    /**
     * Updates the rooms arraylist
     *
     * @param room
     */
    public void updateRooms(Room room) {
        for (int i = 0; i < this.rooms.size(); ++i) {
            if (room.getNum() == this.rooms.get(i).getNum()) {
                this.rooms.set(i, room);
            }
        }
    }


    @Override
    public String toString() {
        return "==== " + this.getName() + " ====\n" +
                "Capacity : " + this.getCapacity() + "\n" +
                "Number of rooms : " + this.getRoomNbr() + "\n";
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getRoomNbr() {
        return roomNbr;
    }

    public void setRoomNbr(int roomNbr) {
        this.roomNbr = roomNbr;
    }
}
