package common;

/**
 * common.CafardPal
 * Heritage : Card
 */
public class CafardPal implements Card {
    private String name; // Name of the owner
    private String codeCarte; // Code of the card

    public CafardPal(String codeCarte, String name) {
        this.name = name;
        this.codeCarte = codeCarte;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeCarte() {
        return codeCarte;
    }

    public void setCodeCarte(String codeCarte) {
        this.codeCarte = codeCarte;
    }
}
