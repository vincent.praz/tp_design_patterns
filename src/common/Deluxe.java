package common;

public class Deluxe extends Room {

    public Deluxe(int num) {
        super(num);
    }

    @Override
    int price() {
        return 75;
    }

    @Override
    boolean douche() {
        return false;
    }

    @Override
    public void printBill() {
        super.printBill();
        System.out.println("Deluxe:\t75");
    }
}