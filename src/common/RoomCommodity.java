package common;

/**
 * RoomCommodity Design Pattern : Decorator
 */
abstract class RoomCommodity extends Room {
    Room room;

    public RoomCommodity(Room room) {
        super(room.getNum());
        this.room = room;
    }
}

class Spa extends RoomCommodity {

    public Spa(Room room) {
        super(room);
    }

    @Override
    public int price() {
        return 20 + this.room.price();
    }

    @Override
    boolean douche() {
        return douche();
    }

    @Override
    public void printBill() {
        this.room.printBill();
        System.out.println("Spa:\t20");
    }
}

class MiniBar extends RoomCommodity {
    public MiniBar(Room room) {
        super(room);
    }

    @Override
    public int price() {
        return 10 + this.room.price();
    }

    @Override
    boolean douche() {
        return douche();
    }

    @Override
    public void printBill() {
        this.room.printBill();
        System.out.println("Mini-Bar:\t10");
    }
}

class Douche extends RoomCommodity {
    public Douche(Room room) {
        super(room);
    }

    @Override
    public int price() {
        return 25 + this.room.price();
    }

    @Override
    boolean douche() {
        return true;
    }

    @Override
    public void printBill() {
        this.room.printBill();
        System.out.println("Douche:\t25");
    }
}