package common;

/**
 * Colony
 * Design Pattern : Observer
 */
public class Colony implements ColonyObserver {
    private String name; // Name of the colony
    private int population; // Population of the colony
    private int growRate; // Grow rate of the colony

    public Colony(String name, int population, int growRate) {
        this.name = name;
        this.population = population;
        this.growRate = growRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public int getGrowRate() {
        return growRate;
    }

    public void setGrowRate(int growRate) {
        this.growRate = growRate;
    }

    @Override
    public void notifier(Room room, Motel motel) {
        System.out.println(this.getName() + " : Puis-je récupérer la chambre " + room.getNum() + " qui s'est libérée ?");
    }

    @Override
    public void subscribe(Motel motel) {
        motel.subscribe(this);
    }
}
