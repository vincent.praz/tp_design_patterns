package common;

public class Suite extends Room {

    public Suite(int num) {
        super(num);
    }

    @Override
    int price() {
        return 100;
    }

    @Override
    boolean douche() {
        return false;
    }

    @Override
    public void printBill() {
        super.printBill();
        System.out.println("Suite:\t100");
    }
}