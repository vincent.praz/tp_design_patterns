package common;

/**
 * Transaction
 */
public class Transaction {
    private Card card; // The card used for the transaction
    private int price; // The price of the transaction
    private String name; // The name of the client

    public Transaction(Card card, int price, String name) {
        this.card = card;
        this.price = price;
        this.name = name;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Type : \t" + this.getCard().getClass().getSimpleName() + "\n" +
                "Price : \t" + this.getPrice() + "\n" +
                "Client : \t" + this.getName() + "\n";
    }
}
