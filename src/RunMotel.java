import common.Colony;
import common.Motel;
import common.Room;

import java.util.ArrayList;

public class RunMotel {
    public static void main(String[] args) {
        Motel mr = Motel.getInstance();
        mr.createRooms();
        System.out.println(mr);

        Colony c1 = new Colony("Première colonie", 100, 200);
        Colony c2 = new Colony("Deuxième colonie", 100, 200);
        Colony c3 = new Colony("Troisième colonie", 100, 200);


        ArrayList<String> commodites = new ArrayList<String>();
        commodites.add("Spa");
        commodites.add("Douche");
        Room r1 = mr.enregistrement(c1, "Suite", commodites);

        // Test if duplicate commodity
        commodites.add("Mini-bar");
        commodites.add("Mini-bar");


        Room r2 = mr.enregistrement(c2, "Luxe", commodites);


        r1.makeFiesta();
        r2.makeFiesta();


        Room r3 = mr.enregistrement(c3, "Luxe", commodites);

        mr.addDays();

        mr.leaveRoom(r1);
        mr.leaveRoom(r2);

        mr.printMotelTransactions();

    }
}